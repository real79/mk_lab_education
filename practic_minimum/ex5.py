from datetime import datetime

date_str = str(input('Please input first date'))
date_str = '05.01.2022 12:00' if not date_str else date_str
date1 = datetime.strptime(date_str, '%d.%m.%Y %H:%M')

date_str = str(input('Please input second date'))
date_str = '21.01.2022 15:00' if not date_str else date_str
date2 = datetime.strptime(date_str, '%d.%m.%Y %H:%M')

date_str = str(input('Please input third date'))
date_str = '20.01.2022 10:00' if not date_str else date_str
date3 = datetime.strptime(date_str, '%d.%m.%Y %H:%M')

print("Yes") if (date1 <= date3 <= date2) else print("No")
