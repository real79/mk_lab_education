from datetime import datetime
from random import randint
from good import Good
from controllers import Controllers, create_controllers_class

date_str = input('please enter date \"DD.MM.YYYY\"')
date_date = datetime.strptime(date_str, '%d.%m.%Y')
goods = []
for i in range(1, 10):
    goods.append(Good(randint(950, 1150)))

create_controllers_class(3)
controllers = Controllers(3)
if divmod(date_date.day, 3)[1] == 0:
    func = getattr(controllers, 'controller3')
elif divmod(date_date.day, 2)[1] == 0:
    func = getattr(controllers, 'controller2')
else:
    func = getattr(controllers, 'controller1')

for good in goods:
    print(f'{good.weight} - {good.is_valid(func)}')