class Good:
    def __init__(self, weight: int):
        self.weight = weight

    def is_valid(self, func):
        return func(self.weight)
