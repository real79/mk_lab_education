from types import FunctionType, MethodType
from os import getenv
import re

Controllers = type('Controllers', (object, ), {'controllers_count': 1})
pattern = re.compile(r'\d{1,}-{1}\d{1,}')


def create_controllers_class(controllers_count):
    code = compile(
        'def __init__(self, controllers_count):\n' +
        '   self.controllers_intervals = []\n' +
        '   for i in range(1, controllers_count + 1):\n' +
        '       env_value = getenv(f"CONTROLLER{i}_INTERVAL", None)\n' +
        '       if not env_value: raise Exception(f\"ENV[CONTROLLER{i}_INTERVAL] has not defined\")\n' +
        '       if re.match(pattern, "env_value"):\n' +
        '           raise Exception(f\"ENV[CONTROLLER{i}_INTERVAL] has not format [\\d*\\-\\d*]\")\n' +
        '       (min_weight, max_weight) = [int(it) for it in env_value.split("-")[:2]]\n' +
        '       self.controllers_intervals.append((min_weight, max_weight))\n',
        '<string>',
        'exec'
    )
    function = FunctionType(code.co_consts[0], globals(), '__init__')
    Controllers.__init__ = MethodType(function, Controllers)

    for i in range(1, controllers_count + 1):
        code = compile(
            f'def controller{i}(self, weight):\n'
            f'  (min_weight, max_weight) = self.controllers_intervals[{i-1}]\n' +
            f'  return min_weight <= weight <= max_weight',
            '<string>',
            'exec'
        )
        function = FunctionType(code.co_consts[0], globals(), f'controller{i}')
        setattr(Controllers, f'controller{i}', MethodType(function, Controllers))
