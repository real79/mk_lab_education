from datetime import datetime
from dateutil.relativedelta import relativedelta


def strong(func):
    def inner(*args, **kwargs):
        return f'<strong>{func(*args, **kwargs)}</strong>'
    return inner


class Human:
    def __init__(self, name: str, lastname: str, birthday: datetime):
        self.name = name
        self.lastname = lastname
        self.birthday = birthday


    def __str__(self) -> str:
        return f'{self.name} {self.lastname}'

    def age(self) -> int:
        return relativedelta(datetime.now(), self.birthday).years


class HumanAgeDays(Human):
    def age(self):
        return (datetime.now() - self.birthday).days

    @strong
    def __str__(self):
        return super().__str__()


human = Human('alex', 'petrov', datetime.fromisoformat('2020-01-01'))
print(human)
print(human.age())

human_age_days = HumanAgeDays('alex', 'petrov', datetime.fromisoformat('2020-01-01'))
print(human_age_days.age())
print(human_age_days)
