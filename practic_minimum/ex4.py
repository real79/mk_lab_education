from datetime import datetime

date_str = str(input('Please input first date'))
date_str = '05.01.2022 10:20' if not date_str else date_str
date1 = datetime.strptime(date_str, '%d.%m.%Y %H:%M')

date_str = str(input('Please input second date'))
date_str = '21.01.2022 12:00' if not date_str else date_str
date2 = datetime.strptime(date_str, '%d.%m.%Y %H:%M')

diff_seconds = int(date2.timestamp() - date1.timestamp())
print(
    f'hours: {divmod(diff_seconds, 3600)[0]}\n' +
    f'days: {(date2 - date1).days}\n' +
    f'minutes: {divmod(diff_seconds, 60)[0]}\n' +
    f'seconds: {diff_seconds}\n'
)
