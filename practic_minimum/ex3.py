from datetime import datetime

date_str = str(input('Please input date'))
date_str = '21.01.2022 20:50' if not date_str else date_str
date_date = datetime.strptime(date_str, '%d.%m.%Y %H:%M')
print(date_date.replace(hour=10).strftime('%d.%m.%Y %H:%M'))
