from datetime import datetime
import calendar

date_str = str(input('Please input date'))
date_str = '21.01.2022' if not date_str else date_str
date_date = datetime.strptime(date_str, '%d.%m.%Y')
date_begin = date_date.replace(day=1)
date_end = date_date.replace(day=calendar.monthrange(date_date.year, date_date.month)[1])
month = date_date.strftime('%b')
print(f'month is {month}: {date_begin.strftime("%d.%m.%Y")} - {date_end.strftime("%d.%m.%Y")}')
