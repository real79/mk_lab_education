import copy

goods = [
    ['good1', 1, 20],
    ['good2', 2, 30],
    ['good2', 3, 50],
    ['good3', 1, 40]
]


def ex6(goods_list: list) -> dict:
    amount = 0
    for good in goods_list:
        amount += good[1] * good[2]
    return {'good': amount}


def ex7(goods_list: list) -> (list, list):
    result1 = []
    result2 = []
    for good in goods_list:
        if not [it[0] for it in result1].__contains__(good[0]):
            result1.append(good)
        elif not [it[0] for it in result2].__contains__(good[0]):
            result2.append(good)
    return result1, result2


def ex8_lists_to_sets(goods_list_1: list, goods_list_2: list) -> (set, set):
    goods_list_1.append(copy.copy(goods_list_2[0]))
    goods_list_2.append(copy.copy(goods_list_1[0]))
    result1 = set(tuple(it) for it in goods_list_1)
    result2 = set(tuple(it) for it in goods_list_2)
    return result1, result2


print(ex6(goods))

(goods1, goods2) = ex7(goods)
print(f'first is  {goods1} and second is {goods2}')

(set_goods1, set_goods2) = ex8_lists_to_sets(goods1, goods2)
print(f'union is {set_goods1 | set_goods2}')
print(f'intersection is {set_goods1 & set_goods2}')

print(sorted(goods, key=lambda it: it[1]*it[2]))
